[EN](README.md) | FR

# humanity-evo
Faite croitre et survivre l'humanité.
D'abord avec l'insouciance d'un jeu de gestion classique, puis face au ravages écologique du culte de la croissance.

## Finalité du projet
- Réaliser un jeu solo libre, jouable en ligne, dont les parties peuvent durer moins d'une heure pour arriver à un game-over (idéalement perdre en moins de 15 minutes pour les pressé serait idéal)
- Commencer avec un paradigme capitaliste pour que le joueur lambda puisse prendre ses marques... Puis le confronter au limite d'une croissance infini dans un monde fini pour le faire remettre en cause notre société... Puis lui montrer qu'il est pris au piège et ne peux plus faire marche arrière... Puis le laisser se débatre avec des outils séduisant mais inneficace, et d'autres radicaux, potentiellement efficace, mais innaplicable du fait de l'innertie culturelle réduisant la capacité à changer profondément de direction... Puis lui montrer les désastres issue du dérèglement brutal du climat, puis la fin du l'humanité.
- Lui proposer de rejouer en prenant des mesures pertinante plus tôt pour sauver l'humanité de sa perte (mais tout de même en ayant à se battre contre une culture capitaliste automate).
- Lui proposer d'approfondir les alternatives qui lui sont proposé tout du long de la partie en lui fournissant des liens vers les sites des ses initiatives (et donc la possibilité de s'y impliquer dans son quotidien de citoyen du monde plutôt que dans humanity-evo).
- Lui proposer de vivre d'autres façon de faire société via des expérimentations social numérique tel que le [jeu ECO](https://play.eco/), des [stage de permaculture](https://miss-permaculture.com/fr/agenda.html), des éco-villages ou tout autre alternatives à vivre.


## Histoire et étapes du jeu

1. Séquence accélérée du big-bang à l'apparition des premiers hommes.

   Méchaniques de jeu : 
   - Réglage de vitesse de jeu
   - Pause/vitesse normale à la survenue d'évènement d'envirgure
   - Réglage par le joueur de l'envirgure des évènements qui déclenche un retour en vitesse normal et de ceux qui déclenche une pause.

2. objectif : maintenir en vie votre tribu et la faire croitre en population.

   Méchaniques de jeu suplémentaire :
   - Répartir les effectifs entre différentes taches pour collecter des ressources (consomation et production affichée, mais pas de stock).
     (Au départ, les seules taches disponnibles sont chasser, pêcher et chercher de la végétation commestible.
     La recherche en débloquera d'autres.)
   - Les effectifs affecté à une tache gagnent en compétences dans celle-ci.
   - Les effectifs oisifs rechercher de nouvelles technologies (selon leur compétences) pour gagner en efficacité et donc mieux survivre et prospérer.
   - Les effectifs oisifs remplissent tout les autres rôles non identifiés et spécialisé (recherche tehchnologique, élever les enfants, transmettre les savoirs, soigner les malades,...) mais rien de tout ça n'est affiché au joueur (si ce n'est les conséquences : découverte technologiques quand elles ont lieux, mort des enfants si personnes ne s'en occupent...).
   - Apparitions des rôles invisible des "oisifs" en tant que rôle spécialisé au fur et à mesure des recherches.

3. objectif : s'outillier pour gagner en efficacitée (simultanée à l'objectif surplus/stock).

   Méchaniques de jeu suplémentaire :
   - produire et s'équiper d'éléments ayant un effet passif, un coup d'entretien et une durée de vie (vetements chaud/résistant, équipement de chasse, de pêche)
   - domestiquer/élever des animaux (apportant aussi des bonus à l'usage, un coup d'entretien et une durée de vie).
     Eux aussi affectable à différentes taches, en autonomie ou en combinaison avec des humains.

4. objectif : produire des surplus pour traverser les aléas sans perte humaines.

   Méchaniques de jeu suplémentaire :
   - Stocker des ressources (avec des effets combo positif ou surtout négatif du stockage en grande quantité (infestations)) avec des plafond de stockage selon équipements.
   - construire (avec la sédentarisation pour monter en échelle) (techniquement des outils complexes, éventuellement avec un lieu si l'aspect géographique est géré dans le jeu).

5. objectif : survivre à la concurence pour préserver sa civilisation face aux autres.

   Méchaniques de jeu suplémentaire :
   - apparition des autres tribu/civilisation ayant fait des choix de développement différents des votres et leur "part de marché" sur la population humaine totale.
   - armement et militaire pour resister aux voisins et les détruire.
   - propagande, pour convertir les voisins et ne pas voir ses effectifs rejoindre les voisins.
   - esclavage/traite humaine, pour faire travailler les voisins pour soi à frais réduit
   - monnaie pour pratiquer l'esclavage avec une meilleur acceptation social (attention, l'upgrade monnaie-libre réduit l'effet esclavagisant)

6. objectif : automatiser culturellement le progrès grace à la finance (ou une IA) qui effectue les choix d'affectation, de priorité de recherche et d'investissement à votre place.

   Méchaniques de jeu suplémentaire :
   - auto-investissement/affectation en fonction de la rentabilité à court terme, ou au plus estimé à l'année voir sur 5 ans. ( à voir comment laisser de la marge de maneuvre ou joueur tout en la réduisant progressivement)

7. objectif : adapter le fonctionnement de la société aux contraintes écologique

   Méchaniques de jeu suplémentaire :
   - Stock naturel désormais visible (Total partagé entre toutes les civilisations, de l'oxigène au pétrole en passant par la surface de forêt)
     - stock (élevé pour le pétrole, vide pour l'énergie solaire),
     - un rythme de production (négliable pour le pétrole, important pour l'énergie solaire)
     - un rythme de d'exploitation/consomation (dépendant de l'usage cumulé de chaque civilisation).
   - Déchet produits désormais visible (CO², pollution des eaux) et indication de leur impacte sur les ressources.
   - Effets secondaires indésirables désormais visibles (allergène, cancérigène)
   - Projet politique, et militant pour faire évoluer les règles et la culture de la civilisation (en se heurtant à l'inertie culturelle et la corruption institutionnelle/inertie des automates capitalistes)

8. objectif : survivre jusqu'à l'absorbsion de la terre par le soleil et/ou coloniser l'univers

   Méchaniques de jeu suplémentaire :
   - proposer du solutionnisme technologique onéreux et inefficace mais qui face rêver. Les calibrer pour qu'il soit efficace dans un monde sans contraites écologique.
   - proposer des approches transhumaniste et surtout post-humaniste (informatisation de l'esprit humain pour dépendre d'électricité et de minéraux au lieux de composé organique recyclable)
   - confronter le monde à la montée des eaux, de la température et autres dérèglement climatique impactant les ressources disponnible à l'humanité.
   - confronter le monde à la disparition de l'eau liquide sur terre si la température monte trop et aux équipement nécessaire pour que quelque-uns y survive, et montrer à quel point l'évolution technologique et les perspective pour l'humanité se retrouvent stoppées nettes

9. Game-over : la mort du dernier humain signe la fin de partie. A moins que ce soit du fait de l'absorbsion de la terre par le soleil, c'est un game-over.

   Méchaniques de jeu suplémentaire :
   - Message de fin : Game over, l'humanité c'est éteinte après x temps d'existence (année Y), N humain.e.s ont pu vivre grace à vous. Saurez-vous faire mieux la prochaine fois ?
   - Le score final N est basé sur la somme de la population au court du temps (avoir maintenu un maximum d'humain.e en vie aux court du temps, hors fases cryogénique si existantes)

10. Et maintenant, comment sauver le monde en vrai ?
    
    Selon les idées et sponsors du jeu, l'écran de game-over sera complété avec ce qui suis :
    - Lien vers les différents alternatives proposées au court du jeu
    - Proposition d'expérimenter de différentes manières [jeu ECO](https://play.eco/), [stage de permaculture](https://miss-permaculture.com/fr/agenda.html), éco-villages...
    - Le jeu ne tien pas compte de tel ou tel aspect et ça permetrait si ou ça ? Le jeu est libre, proposez et/ou forkez pour modifier vous-même !





